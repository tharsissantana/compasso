# compasso
Compasso - Catálogo de Produtos

# Prerequisites
 - Docker
 - JVM 11
 
## Running

Into the root directory execute:

```bash
mvn clean package
docker-compose up
```

## Documentation
```bash
http://localhost:9999/swagger-ui.html
```
