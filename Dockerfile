FROM openjdk:11-jdk-slim

WORKDIR /opt/compasso

COPY /target/compasso*.jar compasso.jar

EXPOSE 8080

CMD java -jar compasso.jar