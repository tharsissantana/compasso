CREATE TABLE product
(
    id uuid NOT NULL,
    name varchar(100) NOT NULL,
    description varchar(500) NOT NULL,
    price numeric NOT NULL,
    PRIMARY KEY (id)
);
