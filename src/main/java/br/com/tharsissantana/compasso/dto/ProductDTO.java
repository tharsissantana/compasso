package br.com.tharsissantana.compasso.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

    @ApiModelProperty(value = "Nome do produto")
    @Getter
    @Setter
    @NotNull
    private String name;

    @ApiModelProperty(value = "Descrição do produto")
    @Getter
    @Setter
    @NotNull
    private String description;

    @ApiModelProperty(value = "Preço do produto")
    @Getter
    @Setter
    @NotNull
    private BigDecimal price;

}
