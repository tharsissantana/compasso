package br.com.tharsissantana.compasso.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@RestControllerAdvice
public class ArgumentNotValidHandler {

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Error handle(MethodArgumentNotValidException expection) {

        List<String> erros = new ArrayList<>();

        List<FieldError> fieldErrors = expection.getBindingResult().getFieldErrors();

        for (FieldError fieldError : fieldErrors) {
            erros.add(fieldError.getField() + ": " + fieldError.getDefaultMessage());
        }

        return new Error(HttpStatus.BAD_REQUEST.value(), erros.toString().replace("[", "").replace("]", ""));
    }

    @AllArgsConstructor
    public class Error {

        @Getter
        @Setter
        private int stastus_code;

        @Getter
        @Setter
        private String message;
    }

}
