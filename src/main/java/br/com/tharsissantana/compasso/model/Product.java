package br.com.tharsissantana.compasso.model;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Product {

    @ApiModelProperty(value = "Id do produto")
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    private UUID id;

    @ApiModelProperty(value = "Nome do produto")
    @Getter
    @Setter
    @NotNull
    private String name;

    @ApiModelProperty(value = "Descrição do produto")
    @Getter
    @Setter
    @NotNull
    private String description;

    @ApiModelProperty(value = "Preço do produto")
    @Getter
    @Setter
    @NotNull
    private BigDecimal price;

}
