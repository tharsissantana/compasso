package br.com.tharsissantana.compasso.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.tharsissantana.compasso.dto.ProductDTO;
import br.com.tharsissantana.compasso.model.Product;
import br.com.tharsissantana.compasso.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @ApiOperation(value = "Criação de um produto", response = Product.class, notes = "Esta operação cria um novo produto ")
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Created"), @ApiResponse(code = 400, message = "Bad request") })
    @PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Product create(@Valid @RequestBody ProductDTO productDTO) {
        return productService.create(productDTO);
    }

    @ApiOperation(value = "Atualização de um produto", response = Product.class, notes = "Esta operação atualiza um novo produto ")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Bad request"), @ApiResponse(code = 404, message = "Product not found") })
    @PutMapping(value = "/products/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Product update(@PathVariable UUID id, @Valid @RequestBody ProductDTO productDTO) {
        return productService.update(id, productDTO);
    }

    @ApiOperation(value = "Busca de um produto por ID", response = Product.class, notes = "Esta operação busca um produto por ID ")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Product not found") })
    @GetMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Product findById(@PathVariable UUID id) {
        return productService.findById(id);
    }

    @ApiOperation(value = "Lista de produtos", response = Product.class, responseContainer = "List", notes = "Esta operação retorna uma lista com todos os produtos ")
    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> listAll() {
        return productService.listAll();
    }

    @ApiOperation(value = "Lista de produtos filtrados", response = Product.class, responseContainer = "List", notes = "Esta operação retorna uma lista de produtos baseado nos filtros informados ")
    @GetMapping(value = "/products/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> search(@RequestParam(required = false) String q, @RequestParam(required = false) Double minPrice, @RequestParam(required = false) Double maxPrice) {
        return productService.search(q, minPrice, maxPrice);
    }

    @ApiOperation(value = "Deleção de um produto", notes = "Esta operação apaga um produto ")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Product not found") })
    @DeleteMapping(value = "/products/{id}")
    public void delete(@PathVariable UUID id) {
        productService.delete(id);
    }

}
