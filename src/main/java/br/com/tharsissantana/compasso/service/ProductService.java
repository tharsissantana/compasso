package br.com.tharsissantana.compasso.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.criteria.Predicate;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.tharsissantana.compasso.dto.ProductDTO;
import br.com.tharsissantana.compasso.exception.NotFoundException;
import br.com.tharsissantana.compasso.model.Product;
import br.com.tharsissantana.compasso.repository.ProductRepository;

@Service
public class ProductService {

    private static final String PRODUCT_NOT_FOUND = "Product not found on :: ";

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    public Product create(ProductDTO productDTO) {
        return productRepository.save(modelMapper.map(productDTO, Product.class));
    }

    public Product update(UUID id, ProductDTO productDTO) throws NotFoundException {
        Product product = productRepository.findById(id).orElseThrow(() -> new NotFoundException(PRODUCT_NOT_FOUND + id));
        product.setName(productDTO.getName());
        product.setDescription(productDTO.getDescription());
        product.setPrice(productDTO.getPrice());

        return productRepository.save(product);
    }

    public Product findById(UUID id) throws NotFoundException {
        return productRepository.findById(id).orElseThrow(() -> new NotFoundException(PRODUCT_NOT_FOUND + id));
    }

    public List<Product> listAll() {
        Iterable<Product> iterable = productRepository.findAll();

        List<Product> result = new ArrayList<>();
        iterable.forEach(result::add);

        return result;
    }

    public List<Product> search(String q, Double minPrice, Double maxPrice) {
        Iterable<Product> iterable = productRepository.findAll(getProductQuery(q, minPrice, maxPrice));

        List<Product> result = new ArrayList<>();
        iterable.forEach(result::add);

        return result;
    }

    public Map<String, Boolean> delete(UUID id) throws NotFoundException {
        Product product = productRepository.findById(id).orElseThrow(() -> new NotFoundException(PRODUCT_NOT_FOUND + id));

        productRepository.delete(product);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;

    }

    private Specification<Product> getProductQuery(String q, Double minPrice, Double maxPrice) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (q != null && !q.trim().isEmpty()) {
                predicates.add(criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), "%" + q.toUpperCase() + "%"),
                        criteriaBuilder.like(criteriaBuilder.upper(root.get("description")), "%" + q.toUpperCase() + "%")));
            }

            if (minPrice != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), minPrice));
            }

            if (maxPrice != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"), maxPrice));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
